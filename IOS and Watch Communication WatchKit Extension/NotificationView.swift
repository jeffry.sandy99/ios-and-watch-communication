//
//  NotificationView.swift
//  IOS and Watch Communication WatchKit Extension
//
//  Created by Jeffry Sandy Purnomo on 13/11/20.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
