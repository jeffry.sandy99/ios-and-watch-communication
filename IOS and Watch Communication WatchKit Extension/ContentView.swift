//
//  ContentView.swift
//  WatchSpeeCue Extension
//
//  Created by Ahmad Rizki Maulana on 02/11/20.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var model = ViewModelWatch()
    @StateObject var timeCounter = TimeCounter()
    
    var body: some View {
        NavigationView {
            VStack {
                if self.model.status == ""{
                    
//                    ForEach(model.arrayOfCue, id: \.self){ cue in
                        Text("Time: \(timeCounter.time)")
                            .padding()
                            .multilineTextAlignment(.center)
//                        Text(cue.cueName)
//                            .padding()
//                            .multilineTextAlignment(.center)
//                        Text(cue.cueEmoji)
//                            .padding()
//                            .multilineTextAlignment(.center)
//                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

class TimeCounter: ObservableObject {
    @Published var time = 0
    
    lazy var timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in self.time += 1 }
    init() { timer.fire() }
}
