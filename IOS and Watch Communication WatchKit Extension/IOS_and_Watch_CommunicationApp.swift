//
//  IOS_and_Watch_CommunicationApp.swift
//  IOS and Watch Communication WatchKit Extension
//
//  Created by Jeffry Sandy Purnomo on 13/11/20.
//

import SwiftUI

@main
struct IOS_and_Watch_CommunicationApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
