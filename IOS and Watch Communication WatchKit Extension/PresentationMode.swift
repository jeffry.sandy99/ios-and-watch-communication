//
//  PresentationMode.swift
//  WatchSpeeCue Extension
//
//  Created by Ahmad Rizki Maulana on 11/11/20.
//

import SwiftUI

struct PresentationMode: View {
    
    @ObservedObject var model = ViewModelWatch()
    
    @State private var present = false
    @State private var pause = false
    
    @State var cueTime = 90
    @State private var countdown = 3
    @State private var cueTimeDown = 90
    @State var cueTimeUp = 0
    @State private var elapsedTime = 0
    
    let timer = Timer.publish(every: 1, on: .main, in: .common)
        .autoconnect()
    
    var body: some View {
        GeometryReader { geo in
            let geoW = geo.size.width
            let geoH = geo.size.height
            
            if present == false{
                ZStack(alignment: .top) {
                    Rectangle()
                        .foregroundColor(Color(#colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)))
                        .edgesIgnoringSafeArea(.bottom)
                    VStack{
                        ZStack {
                            Circle()
                                .frame(width: (geoW/10)*6, height: (geoW/10)*6, alignment: .center)
                                .foregroundColor(Color(#colorLiteral(red: 0.462745098, green: 0.6235294118, blue: 0.6588235294, alpha: 1)))
                            Circle()
                                .padding()
                                .frame(width: (geoW/10)*6,height: (geoW/10)*6, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                .foregroundColor(Color(#colorLiteral(red: 0.6784313725, green: 0.862745098, blue: 0.9019607843, alpha: 1)))
                            Text("\(countdown)")
                                .foregroundColor(Color(#colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)))
                                .fontWeight(.semibold)
                                .font(.largeTitle)
                        }
                        .frame(width: geoW, height: (geoH/10)*7, alignment: .center)
                        .padding(.top)
                        
                        Button(action: {
                            print("watch Cancel PresentMode")
                        }, label: {
                            Text("Cancel")
                                .foregroundColor(Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
                        })
                        .background(Color(#colorLiteral(red: 0.9035397172, green: 0.2593905926, blue: 0.1568938494, alpha: 1)))
                        .padding()
                        .frame(width: geoW, height: (geoH/10)*3, alignment: .center)
                        .clipShape(Capsule())
                    }
                    .onReceive(timer, perform: { time in
                        if self.countdown > 0{
                            self.countdown -= 1
                        }else if self.countdown == 0{
                            self.present = true
                        }
                    })
                }
                .onAppear(perform: {
                    self.model.session.sendMessage(["watchMessage":"Start"], replyHandler: nil){ (error) in
                        print(error.localizedDescription)
                    }
                })
                .edgesIgnoringSafeArea(.bottom)
                
            }else{
                ZStack(alignment: .top){
                    //MARK: FIRST LAYER -
                    //                    Rectangle()
                    //                        .foregroundColor(Color(#colorLiteral(red: 0.262745098, green: 0.3960784314, blue: 0.4196078431, alpha: 1)))
                    //                        .edgesIgnoringSafeArea(.bottom)
                    //
                    //MARK: SECOND LAYER -
                    //                    backgroundPresentMode(geoH: geoH)
                    ScrollView(.horizontal){
                        HStack(alignment: .top){
                            TabView {
                                ForEach(0..<5){i in
                                    ZStack {
                                        Rectangle()
                                            .foregroundColor(Color(#colorLiteral(red: 0.262745098, green: 0.3960784314, blue: 0.4196078431, alpha: 1)))
                                        cueScreen(textCue: "\(String(i))",textCueNext: "\(String(i+1))")
                                            //                                        .padding()
                                            .frame(alignment: .top)
                                        //                                        .edgesIgnoringSafeArea(.top)
                                    }
                                    .frame(alignment: .top)
                                    .edgesIgnoringSafeArea(.top)
                                    
                                }
                            }
                            .edgesIgnoringSafeArea(.top)
                            .tabViewStyle(PageTabViewStyle())
                            //                            .frame(maxWidth: geoW, alignment: .center)
                        }
                        .frame(width: geoW)
                    }
                    .animation(.linear)
                    .edgesIgnoringSafeArea(.bottom)
                    //                    VStack {
                    //                        ZStack {
                    //                            Rectangle()
                    //                                .foregroundColor(Color(#colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)))
                    //                                .frame(minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
                    //                            Image("Rectangle")
                    //                                .resizable()
                    //                                .aspectRatio(contentMode: .fill)
                    //                                .clipped()
                    //                                .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
                    //
                    ////                            VStack {
                    ////                                Spacer()
                    ////                                    .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3)
                    ////                                Text("😘😘😘")
                    ////                                    .fitSystemFont()
                    ////                                    .padding(.horizontal)
                    ////                                    .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
                    ////                                //                        Text("123456789 123456789 123456789 ")
                    ////                                //                            .fitSystemFont()
                    ////                                //                            .font(.title3)
                    ////                                //                            .padding(.horizontal)
                    ////                                //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
                    ////                            }
                    //                        }
                    //                        .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8, alignment: .top)
                    //                        Text("Next")
                    //                            .fitSystemFont()
                    //                            .frame(minHeight:(geoH/10)/1.5, maxHeight: (geoH/10)/1.5)
                    //                        ZStack {
                    //                            Rectangle()
                    //                                .foregroundColor(Color(#colorLiteral(red: 0.262745098, green: 0.3960784314, blue: 0.4196078431, alpha: 1)))
                    //                                .frame(minHeight:(geoH/10)*3, maxHeight: (geoH/10)*3)
                    //                            Image("Rectangle")
                    //                                .resizable()
                    //                                .aspectRatio(contentMode: .fill)
                    //                                .clipped()
                    //                                .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
                    //                        }
                    //                        .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
                    //                    }
                    //                    .frame(alignment: .top)
                    
                    //MARK: THIRD LAYER -
                    //                    VStack{
                    //                        //                        Image("Rectangle")
                    //                        //                            .resizable()
                    //                        //                            .aspectRatio(contentMode: .fill)
                    //                        //                            .clipped()
                    //                        //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
                    //                        Spacer()
                    //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3)
                    //                        //                        Text("😘😘😘")
                    //                        //                            .fitSystemFont()
                    //                        //                            .padding(.horizontal)
                    //                        //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
                    //                        Text("123456789 123456789 123456789 ")
                    //                            .fitSystemFont()
                    //                            .font(.title3)
                    //                            .padding(.horizontal)
                    //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
                    //                        Text("")
                    //                            .fitSystemFont()
                    //                            .frame(minHeight:(geoH/10)/1.5, maxHeight: (geoH/10)/1.5)
                    //                        //                        Image("Rectangle")
                    //                        //                            .resizable()
                    //                        //                            .aspectRatio(contentMode: .fill)
                    //                        //                            .clipped()
                    //                        //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
                    //                        //                        Text("🙃🙃🙃")
                    //                        //                            .fitSystemFont()
                    //                        //                            .padding(.horizontal)
                    //                        //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*2, maxHeight: (geoH/10)*2)
                    //                        Text("123456789 123456789 123456789 ")
                    //                            .fitSystemFont()
                    //
                    //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*2, maxHeight: (geoH/10)*2)
                    //                    }
                    //                    .frame(alignment: .top)
                    
                    //MARK: FOURTH LAYER -
                    sectionScreen(cueTime: self.cueTime, cueTimeUp: self.cueTimeUp)
                    //                    VStack {
                    //                        Spacer()
                    //                            .frame(maxHeight: (geoH/10)*1)
                    //
                    //                        ZStack {
                    //                            ZStack(alignment: .leading){
                    //                                Rectangle()
                    //                                    .frame(width: (geoW/10)*5, height: (geoH/10)*1.5, alignment: .center)
                    //                                    .foregroundColor(Color(#colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)))
                    //                                if cueTime/2 >= cueTimeUp{
                    //                                    Rectangle()
                    //                                        .frame(maxWidth: ((((geoW/10)*5)/CGFloat(cueTime))*CGFloat(cueTimeUp)), maxHeight: (geoH/10)*1.5, alignment: .leading)
                    //                                        .foregroundColor(Color(#colorLiteral(red: 0.6274509804, green: 0.8666666667, blue: 0.9098039216, alpha: 1)))
                    //                                        .animation(.linear)
                    //                                }else{
                    //                                    Rectangle()
                    //                                        .frame(maxWidth: ((((geoW/10)*5)/CGFloat(cueTime))*CGFloat(cueTimeUp)), maxHeight: (geoH/10)*1.5, alignment: .leading)
                    //                                        .foregroundColor(Color(#colorLiteral(red: 0.9035397172, green: 0.2593905926, blue: 0.1568938494, alpha: 1)))
                    //                                        .animation(.linear)
                    //                                }
                    //                            }.clipShape(Capsule())
                    //
                                                HStack{
                    
                                                    let timeMinute = elapsedTime/60
                                                    let timeSecond = elapsedTime%60
                    
                                                    if timeMinute >= 10{
                                                        Text("\(timeMinute)")
                                                            .font(.title3)
                                                    }else if timeMinute < 10{
                                                        Text("0\(timeMinute)")
                                                            .font(.title3)
                                                    }
                                                    Text(":")
                                                        .font(.title3)
                                                    if timeSecond >= 10{
                                                        Text("\(timeSecond)")
                                                            .font(.title3)
                                                    } else if timeSecond < 10{
                                                        Text("0\(timeSecond)")
                                                            .font(.title3)
                                                    }
                                                }
                    //                        }
                    //
                    //                        Spacer()
                    //                    }
                    //                    .frame(alignment: .top)
                    
                    //MARK: FIFTH LAYER -
                    if pause == true{
                        pauseScreen()
                    }
                }
                .onReceive(timer, perform: { time in
                    if pause == false{
                        countingDown()
                    }
                })
                .onTapGesture(count: 2) {
                    print("Double tapped!")
                    if pause == false{
                        pause = true
                    }else{
                        pause = false
                    }
                }
            }
        }
        .navigationBarBackButtonHidden(true)
    }
    
    func countingDown() {
        if self.cueTimeDown > 0{
            self.cueTimeDown -= 1
            self.cueTimeUp += 1
            self.elapsedTime += 1
        }else if self.cueTimeDown == 0{
            self.cueTimeDown -= 1
            self.elapsedTime += 1
            
        }else if self.cueTimeDown < 0{
            self.elapsedTime += 1
        }
    }
}

struct PresentationMode_Previews: PreviewProvider {
    static var previews: some View {
        PresentationMode()
    }
}


struct FitSystemFont: ViewModifier {
    var lineLimit: Int
    var minimumScaleFactor: CGFloat
    var percentage: CGFloat
    
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .multilineTextAlignment(.center)
                .font(.system(size: min(geometry.size.width, geometry.size.height) * percentage))
                .lineLimit(self.lineLimit)
                .minimumScaleFactor(self.minimumScaleFactor)
                .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
        }
    }
}

extension View {
    func fitSystemFont(lineLimit: Int = 3, minimumScaleFactor: CGFloat = 0.01, percentage: CGFloat = 1) -> ModifiedContent<Self, FitSystemFont> {
        return modifier(FitSystemFont(lineLimit: lineLimit, minimumScaleFactor: minimumScaleFactor, percentage: percentage))
    }
    
}

//struct backgroundPresentMode: View {
//
//    @State private var geoH:CGFloat = 0
//
//    var body: some View {
//        GeometryReader{ geo in
////            let geoW = geo.size.width
//
//
//            VStack {
//                Rectangle()
//                    .foregroundColor(Color(#colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)))
//                    .frame(minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
//                Text("Next")
//                    .fitSystemFont()
//                    .frame(minHeight:(geoH/10)/1.5, maxHeight: (geoH/10)/1.5)
//                Rectangle()
//                    .foregroundColor(Color(#colorLiteral(red: 0.262745098, green: 0.3960784314, blue: 0.4196078431, alpha: 1)))
//                    .frame(minHeight:(geoH/10)*3, maxHeight: (geoH/10)*3)
//            }
//            .frame(alignment: .top)
//        }
//    }
//}

struct pauseScreen: View {
    var body: some View{
        GeometryReader { geo in
            
            let geoW = geo.size.width
            let geoH = geo.size.height
            
            ZStack{
                Rectangle()
                    .opacity(0.7)
                    .foregroundColor(Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)))
                    .frame(width: geoW, height: (geoH/10)*12)
                
                VStack {
                    //                                                    Text("Double Tap to Play")
                    //                                                    Text("Double Tap to Play")
                    Spacer()
                        .frame(maxHeight: (geoH/10)*1, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Image(systemName: "play.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .foregroundColor(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)))
                        .frame(maxWidth: (geoH/10)*1.5, minHeight: (geoH/10)*1.5, maxHeight: (geoH/10)*2, alignment: .center)
                    Spacer()
                        .frame(maxHeight: (geoH/10)*3, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Button(action: {
                        print("watch End Presentation")
                    }, label: {
                        Text("End Presentation")
                            .foregroundColor(Color(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)))
                            .fitSystemFont()
                    })
                    .background(Color(#colorLiteral(red: 0.9882352941, green: 0.168627451, blue: 0.06274509804, alpha: 1)))
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .frame(width: (geoW/10)*8, height: (geoH/10)*2, alignment: .center)
                    .padding()
                    //                            .frame(width: geoW, height: (geoH/10)*2)
                    //                            ZStack {
                    //                                Rectangle()
                    //                                    .frame(width: geoW, height: (geoH/10)*3)
                    //                                Text("Stop")
                    //                                    .frame(width: geoW, height: (geoH/10)*3)
                    //                                    .fitSystemFont()
                    //                            }
                }
                .frame(width: geoW, height: geoH, alignment: .top)
            }
        }
    }
}

struct cueScreen:View {
    
    @State var textCue: String = ""
    @State var textCueNext: String = ""
    
    var body: some View{
        GeometryReader{geo in
            
            let geoW = geo.size.width
            let geoH = geo.size.height
            
            VStack {
                ZStack {
                    Rectangle()
                        .foregroundColor(Color(#colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)))
                        .frame(minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
                    //                        Image("Rectangle")
                    //                            .resizable()
                    //                            .aspectRatio(contentMode: .fill)
                    //                            .clipped()
                    //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
                    
                    VStack {
                        Spacer()
                            .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3)
                        //                        Text("😘😘😘")
                        //                            .fitSystemFont()
                        //                            .padding(.horizontal)
                        //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
                        Text(textCue)
                            .fitSystemFont()
                            .font(.title3)
                            .padding(.horizontal)
                            .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
                    }
                }
                .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8, alignment: .top)
                Text("Next")
                    .fitSystemFont()
                    .frame(minHeight:(geoH/10)/1.5, maxHeight: (geoH/10)/1.5)
                ZStack {
                    //                    Image("Rectangle")
                    //                        .resizable()
                    //                        .aspectRatio(contentMode: .fill)
                    //                        .clipped()
                    //                        .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
                    //                    Text("🙃🙃🙃")
                    //                        .fitSystemFont()
                    //                        .padding(.horizontal)
                    //                        .frame(maxWidth: geoW,minHeight: (geoH/10)*2, maxHeight: (geoH/10)*2, alignment: .top)
                    
                    Text(textCueNext)
                        .fitSystemFont()
                        .frame(maxWidth: geoW,minHeight: (geoH/10)*2, maxHeight: (geoH/10)*2)
                    
                }
                .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
            }
            //                .padding(.horizontal)
            .frame(maxWidth: geoW,maxHeight:geoH,alignment: .center)
            
            
            //            Image("Rectangle")
            //                .resizable()
            //                .aspectRatio(contentMode: .fill)
            //                .clipped()
            //                .frame(width: geoW, height: geoH, alignment: .center)
            //                Image("Rectangle")
            //                    .frame(width: geoW/2, height: geoH, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            
        }
    }
}

struct sectionScreen:View {
    
    @State var cueTime:Int = 1
    @State var cueTimeUp:Int = 1
//    @State var elapsedTime:CGFloat = 1
    
    var body: some View{
        GeometryReader{ geo in
            let geoW = geo.size.width
            let geoH = geo.size.height
            
            VStack {
                Spacer()
                    .frame(maxHeight: (geoH/10)*1)
                
                ZStack {
                    ZStack(alignment: .leading){
                        Rectangle()
                            .frame(width: (geoW/10)*5, height: (geoH/10)*1.5, alignment: .center)
                            .foregroundColor(Color(#colorLiteral(red: 0.568627451, green: 0.568627451, blue: 0.568627451, alpha: 1)))
                        if cueTime/2 >= cueTimeUp{
                            Rectangle()
                                .frame(maxWidth: (((geoW/10)*5)/CGFloat(cueTime))*CGFloat(cueTimeUp), maxHeight: (geoH/10)*1.5, alignment: .leading)
                                .foregroundColor(Color(#colorLiteral(red: 0.6274509804, green: 0.8666666667, blue: 0.9098039216, alpha: 1)))
                                .animation(.linear)
                        }else{
                            Rectangle()
                                .frame(maxWidth: (((geoW/10)*5)/CGFloat(cueTime))*CGFloat(cueTimeUp), maxHeight: (geoH/10)*1.5, alignment: .leading)
                                .foregroundColor(Color(#colorLiteral(red: 0.9035397172, green: 0.2593905926, blue: 0.1568938494, alpha: 1)))
                                .animation(.linear)
                        }
                    }.clipShape(Capsule())
                }
                
                Spacer()
            }
            .frame(alignment: .top)
            .onAppear(perform:{
                print("Testing")
            })
            
        }
    }
}

//VStack {
//    ZStack {
//        Rectangle()
//            .foregroundColor(Color(#colorLiteral(red: 0.2823529412, green: 0.2823529412, blue: 0.2823529412, alpha: 1)))
//            .frame(minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
//        Image("Rectangle")
//            .resizable()
//            .aspectRatio(contentMode: .fill)
//            .clipped()
//            .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8)
//
//        //                            VStack {
//        //                                Spacer()
//        //                                    .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3)
//        //                                Text("😘😘😘")
//        //                                    .fitSystemFont()
//        //                                    .padding(.horizontal)
//        //                                    .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
//        //                                //                        Text("123456789 123456789 123456789 ")
//        //                                //                            .fitSystemFont()
//        //                                //                            .font(.title3)
//        //                                //                            .padding(.horizontal)
//        //                                //                            .frame(maxWidth: geoW,minHeight: (geoH/10)*5, maxHeight: (geoH/10)*5)
//        //                            }
//    }
//    .frame(maxWidth: geoW,minHeight: (geoH/10)*8, maxHeight: (geoH/10)*8, alignment: .top)
//    Text("Next")
//        .fitSystemFont()
//        .frame(minHeight:(geoH/10)/1.5, maxHeight: (geoH/10)/1.5)
//    ZStack {
//        Rectangle()
//            .foregroundColor(Color(#colorLiteral(red: 0.262745098, green: 0.3960784314, blue: 0.4196078431, alpha: 1)))
//            .frame(minHeight:(geoH/10)*3, maxHeight: (geoH/10)*3)
//        Image("Rectangle")
//            .resizable()
//            .aspectRatio(contentMode: .fill)
//            .clipped()
//            .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
//    }
//    .frame(maxWidth: geoW,minHeight: (geoH/10)*3, maxHeight: (geoH/10)*3, alignment: .top)
//}
//.padding()
//.frame(maxWidth: geoW,alignment: .top)
