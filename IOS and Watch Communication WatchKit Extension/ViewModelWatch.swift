//
//  ViewModelWatch.swift
//  WatchSpeeCue Extension
//
//  Created by Ahmad Rizki Maulana on 03/11/20.
//

import Foundation
import WatchConnectivity

class ViewModelWatch : NSObject,  WCSessionDelegate, ObservableObject{
    var session: WCSession
    @Published var status = ""
//    @Published var cuesesData: [CueData] = []
    @Published var cuesesData: CueData = CueData(cueID: "Test", cueImageData: "", targetedTime: "", cueEmoji: "", cueType: "", cueName: "")
    @Published var arrayOfCue: [CueData] = []
    
    init(session: WCSession = .default){
        self.session = session
        super.init()
        self.session.delegate = self
        session.activate()
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
        print("message from phone \(message)")
        DispatchQueue.main.async {
            self.status = message["message"] as? String ?? "Unknown"
        }
        
    }
    
    func session(_ session: WCSession, didReceiveMessageData messageData: Data) {
        do {
            // Decode data to object
            let jsonDecoder = JSONDecoder()
            let cueses = try jsonDecoder.decode(CueData.self, from: messageData)
            
            DispatchQueue.main.async {
                    //update UserAuth().isLoggedIn to TRUE
                self.arrayOfCue.append(cueses)
            }
            
//            print("cueID : \(upMovie.name)")
//            print("cueImageData : \(upMovie.rating)")
//            print("targetedTime : \(upMovie.name)")
//            print("cueEmoji : \(upMovie.rating)")
//            print("cueType : \(upMovie.name)")
//            print("cueName : \(upMovie.rating)")
        }
        catch {
            print("Error Decoding")
        }
    }
    
}

//struct CueDataSend{
//    func printStruct(data: CueData) -> [CueData]{
//
//        return "Total Payment: \(total)"
//    }
//}

struct CueData: Decodable, Hashable{
    let cueID: String
    let cueImageData: String //"" atau ImageData
    let targetedTime: String //
    let cueEmoji: String //"" atau emoji
    let cueType: String //"Introduction" , "Body", "Conclusion"
    let cueName: String // CueText
}

//MARK:- Introduction
//CueData(
//    cueID: "Cue 1",
//    cueImageData: "",
//    targetedTime: "30",
//    cueEmoji: "😰",
//    cueType: "Introduction",
//    cueName: "Dont Give Up")
//CueData(
//    cueID: "Cue 2",
//    cueImageData: "",
//    targetedTime: "30",
//    cueEmoji: "💓",
//    cueType: "Introduction",
//    cueName: "Stay Loved")

//MARK:- Body
//CueData(
//    cueID: "Cue 3",
//    cueImageData: "",
//    targetedTime: "40",
//    cueEmoji: "😰",
//    cueType: "Body",
//    cueName: "Stay Loved")
 
