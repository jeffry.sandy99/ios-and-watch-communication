//
//  IOS_and_Watch_CommunicationApp.swift
//  IOS and Watch Communication
//
//  Created by Jeffry Sandy Purnomo on 13/11/20.
//

import SwiftUI

@main
struct IOS_and_Watch_CommunicationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
