//
//  ViewModelPhone.swift
//  Speecue
//
//  Created by Ahmad Rizki Maulana on 03/11/20.
//

import Foundation
import WatchConnectivity

class ViewModelPhone : NSObject,  WCSessionDelegate, ObservableObject{
    var session: WCSession
    @Published var watchMessage = ""
    
    init(session: WCSession = .default){
        self.session = session
        super.init()
        self.session.delegate = self
        session.activate()
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("session did become inactive")
        self.session.sendMessage(["message":""], replyHandler: nil){ (error) in
            print(error.localizedDescription)
        }
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("session did deactivate")
        self.session.sendMessage(["message":""], replyHandler: nil){ (error) in
            print(error.localizedDescription)
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]){
        DispatchQueue.main.async {
            self.watchMessage = message["watchMessage"] as? String ?? "Unknown"
        }
    }
}
