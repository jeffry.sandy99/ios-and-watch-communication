//
//  ContentView.swift
//  IOS and Watch Communication
//
//  Created by Jeffry Sandy Purnomo on 13/11/20.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var model = ViewModelPhone()
    var cuesesRaw: [CueData] = [
        CueData(
            cueID: "Cue 1",
            cueImageData: String(data: Data("Mountain".utf8) , encoding: .utf8) ?? "",
            targetedTime: "40",
            cueEmoji: "😱💓😰",
            cueType: "Introduction",
            cueName: "Scared to Love"),
        CueData(
            cueID: "Cue 2",
            cueImageData: String(data: Data("River".utf8) , encoding: .utf8) ?? "",
            targetedTime: "30",
            cueEmoji: "💪🤕",
            cueType: "Body",
            cueName: "Flows like a River")
    ]
    
    var body: some View {
        VStack{
            Text("Hello, world!")
                .padding()
                .onTapGesture(perform: {
                    print("IOS")
                    let jsonEncoder = JSONEncoder()
                    for cue in cuesesRaw{

                        do {
                            let jsonData = try jsonEncoder.encode(cue)
                            self.model.session.sendMessageData(jsonData , replyHandler: nil) { (Error) in
                                print(Error.localizedDescription)
                            }
                        }
                        catch {
                            print("Fail to Send Data to Watch")
                        }
                    }
                })
                .frame(width: 200, height: 50)
                .background(Color.blue)
//            Button("Send Bro"){
//                let jsonEncoder = JSONEncoder()
//                for cue in cuesesRaw{
//
//                    do {
//                        let jsonData = try jsonEncoder.encode(cue)
//                        self.model.session.sendMessageData(jsonData , replyHandler: nil) { (Error) in
//                            print(Error.localizedDescription)
//                        }
//                    }
//                    catch {
//                        print("Fail to Send Data to Watch")
//                    }
//                }
//            }
            .buttonStyle(PlainButtonStyle())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CueData: Codable, Hashable{
    let cueID: String
    let cueImageData: String
    let targetedTime: String
    let cueEmoji: String
    let cueType: String
    let cueName: String
}

//                    let jsonString = String(data: jsonData, encoding: .utf8)
//                    print("JSON String : " + jsonString!)
